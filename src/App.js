import './App.css';

import {useState, useEffect} from 'react'
import {Auth, API} from 'aws-amplify';

function App() {

  const [user, setUser] = useState(null);

  useEffect(() => {
    console.log(user);
    if (user && !user.Session) {
      console.log("Login Done");
    }

  }, [user]);

  useEffect(() => {
    (async () => {
      try {
        console.log('Auto Signin Attempted')
        setUser(await Auth.currentAuthenticatedUser())
        const credential = await Auth.currentCredentials();
        console.log(credential);
        console.log("accessKeyId", credential['accessKeyId'])
        console.log("secretAccessKey", credential['secretAccessKey'])
        console.log("sessionToken", credential['sessionToken'])
      } catch (error) {
        console.log('Auto SIgn in Issue : ', error)
      }
    })();
  }, [])

  return (
    <div className="App">
      <header className="App-header">
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <button onClick={async () => {
          console.log("Login Initiated");
          setUser(await Auth.signIn("+918670500969"));
        }}>Request OTP</button>
        <button onClick={async () => {
          console.log("Entering OTP");
          setUser(await Auth.sendCustomChallengeAnswer(user, "1234"));
        }}>Enter OTP</button>
        <button onClick={async () => {
          console.log("Entering Wrong OTP");
          setUser(await Auth.sendCustomChallengeAnswer(user, "1235"));
        }}>Enter Wrong OTP</button>
        <button onClick={async () => {
          console.log(await API.post("backend", "/work/toy", {
            body: {
              test: "data"
            }
          }))
        }}>Call API</button>
        <button onClick={async () => {
          console.log("Log Out Clicked");
          await Auth.signOut();
          setUser(null);
        }}>Log Out</button>
      </header>
    </div>
  );
}

export default App;
