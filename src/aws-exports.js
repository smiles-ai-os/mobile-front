const awsmobile = {
    "Auth": {
        "userPoolId": "ap-south-1_RS17SXbc4",
        "userPoolWebClientId": "gqm8o1pq3ihi267c0vqa1i2c0",
        "identityPoolId": "ap-south-1:4815bc1c-ce4e-41af-9e1f-aac74e4b8987",
        "region": "ap-south-1",
        "authenticationFlowType": "CUSTOM_AUTH"
    },
    "API": {
        "endpoints": [
            {
                "name": "backend",
                "endpoint": "https://qy88y4qwkk.execute-api.ap-south-1.amazonaws.com/dev",
                "region": "ap-south-1"
            }
        ]
    },
};

export default awsmobile;
